$(function() {
    var openModal = function() {
        $(".modal").addClass("active");
        $(".layoutModal").addClass("active");
    };
    var closeModal = function() {
        $(".modal").removeClass("active");
        $(".layoutModal").removeClass("active");
    };


    $(".center .white").click(openModal);
    $(".layoutModal").click(closeModal);
    $(".modal .sprite-close").click(closeModal);

    var contentWidth;
    var width;
    var menu;

    $(".menu").click(function() {
        contentWidth = 1140;
        width = $(window).width();
        menu = (width-contentWidth)/2;
        if(menu > 370) {
            $(".slideMenu").css({
                left: 0,
                width: menu
            });
        } else {
            $(".slideMenu").addClass("open");
            $(".inner .content").addClass("open");
            $("body").css({
               overflow: "hidden"
            });
        }
    });

    $(".slideMenu .sprite-close").click(function() {
        sideMenuClose();
    });

    var sideMenuClose = function() {
        if(menu > 370) {
            $(".slideMenu").css({
                left: -menu
            });
        } else {
            $(".slideMenu").removeClass("open");
            $(".inner .content").removeClass("open");
            $("body").css({
                overflow: "initial"
            });
        }
    };

    $(".slick").slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
        dotsClass: "dots",
        arrows: false
    });

    $(".sliderGoods").slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        dotsClass: "dots",
        prevArrow: '<div class="sliderPrev sprite sprite-arrow-prev"></div>',
        nextArrow: '<div class="sliderNext sprite sprite-arrow-next"></div>'
    });
    $("input[type=tel]").mask("+7 (999) 999-99-99");

    var tumbler = true;
    $(".review-btn").click(function() {
        var reviews = $(".reviews");
        if(tumbler) {
            reviews.addClass("active");
            tumbler = false;
            $(this).html("CКРЫТЬ");
        } else {
            reviews.removeClass("active");
            tumbler = true;
            $(this).html("ЧИТАТЬ ЕЩЕ");
        }
    });

    var rating = $(".rating .star");
    $(".rating").on("mouseover", ".star", function() {
       var n = rating.index(this);
        n = n-19;
        $(".star").removeClass("active");
        for (var i = 1; i <= n; i++) {
            $(".star:nth-child("+i+")").addClass("active");
        }
    });
    $(".rating").on("click", ".star", function() {
        var n = rating.index(this);
        n = n-19;
        $(".star").removeClass("active");
        for (var i = 1; i <= n; i++) {
            $(".star:nth-child("+i+")").addClass("active");
        }
        $(".rating").off("mouseover");
        setTimeout(function() {
            $(".rating").on("mouseover", ".star", function() {
                var n = rating.index(this);
                n = n-19;
                $(".star").removeClass("active");
                for (var i = 1; i <= n; i++) {
                    $(".star:nth-child("+i+")").addClass("active");
                }
            });
        }, 1500);
    });

    $(".plusminus").click(function() {
        var colcol = $(".colcol").html();
        if($(this).hasClass("plus")) {
            colcol++;
        } else {
            colcol--;
            if(colcol < 2) colcol = 1;
        }
        $(".colcol").html(colcol);
    });

    $(".accordion > ul > li").click(function() {
        if($(this).hasClass("active")) {
            $(this).removeClass("active");
        } else {
            $(this).addClass("active");
        }
    });
    $(".accordion > ul > li > ul > li").click(function() {
        return false;
    });
});